package io.sanger.henry.secondmemory;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFileChooser;

import io.sanger.henry.secondmemory.data.Settings;
import io.sanger.henry.secondmemory.question.Question;
import io.sanger.henry.secondmemory.question.QuestionList;
import io.sanger.henry.secondmemory.question.QuestionType;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.fusesource.jansi.AnsiConsole;
import org.json.JSONArray;
import org.json.JSONObject;

import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;

/**
 * <strong><code>SecondMemory</code></strong> is a useful command-line program
 * which helps you to learn and remember things.
 * It <strike>uses spaced repetition</strike> will use spaced repetition. 
 * Spaced repetition makes it easy to remember all kinds of things.<br><br>
 * SecondMemory supports fact questions, which are like questions in
 * similar programs. It requires you to type out answers, which makes it unique.
 * 
 * @author gitlabcyclist
 * @version 0.4 beta
*/
public class SecondMemory {
	
	public static QuestionList list = new QuestionList(); // Master list of questions
	
	public static String floatText = ""; // Text to be shown after the task list.
	
	public static boolean floatTextErroneous = false; // Determines whether the float text should be red or not 	
	
	public static boolean floatTextSuccess = false; // Determines whether the float text should be green or not
	
	public static void main(String[] args) {
		
		// Enable anti-aliasing (thanks to https://batsov.com/articles/2010/02/26/enable-aa-in-swing/)
		System.setProperty("awt.useSystemAAFontSettings", "on");
		System.setProperty("swing.aatext", "true");

		
		// Configure SLF4J to disable message
		BasicConfigurator.configure();
		
		// Disable Log4J to turn off Natty messages
		@SuppressWarnings("unchecked")
		List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
		loggers.add(LogManager.getRootLogger());
		for (Logger logger : loggers) {
		    logger.setLevel(Level.OFF);
		}
		
		// Allows override of the directory with questions.json, settings.json, and topics.json.
		if(Utils.existsInCWD("confDir.txt")) { // If confDir.txt exists, then...
			try { // Make sure to catch any exceptions that might occur from invalid directory, etc.
				String folder = Utils.readFileInCWD("confDir.txt").trim();
				Utils.CWD = folder; // Get contents of confDir.txt
				new File(Utils.CWD).mkdirs(); // Create directory and all subdirectories if needed
				if(Utils.CWD.endsWith(File.separator)) Utils.CWD = Utils.CWD.substring(0, Utils.CWD.length() - 1); // If needed, remove the separator from the end of CWD
				Utils.CWDS = Utils.CWD + File.separator; // Change CWDS accordingly
			} catch(Exception e) {
				Utils.basicRedPrint("There was a problem loading the configuration. Check if the path in confDir.txt is valid.");
				Utils.error(e);
				System.exit(1);
			}
		}
		
		// Use home directory if running on Windows (files won't save if SecondMemory is run from a Program Files folder otherwise).
		// Takes priority over overrideConfDir file.
		if(args.length > 0 && args[0].equals("--use-home-dir")) {
			String userHome = System.getProperty("user.home");
			String secondMemoryFolderPath = userHome + File.separator + ".secondmemory";
			if(!Utils.exists(secondMemoryFolderPath)) Utils.createFolder(secondMemoryFolderPath);
			Utils.CWD = secondMemoryFolderPath;
			Utils.CWDS = Utils.CWD + File.separator;
		}
		
		// Initialize ANSI support only on Windows; Mac & Linux have it by default
		Utils.osDependent(() -> AnsiConsole.systemInstall(), () -> {});
		
		//list.questions = generateSampleData(32, 1533); // uncomment for sample data
		
		boolean intro = false; // When set to true, activates intro 
		
		// Preliminary checks //
		
		// Check for settings.json
		if(!Utils.existsInCWD("settings.json")) {
			// If it doesn't exist, create new (default) settings file
			Utils.createFileInCWD("settings.json");
			try {
				Utils.populateFileInCWD("settings.json", "{ \"questionListTopChar\": \"-\", \"questionListSideChar\": \"|\", "
												 + "\"prompt\": \"->\", \"showCommands\": true, \"showWorkload\": false, "
												 + "\"autoMoveOn\": false, \"questionLimit\": 25, \"useBoxDrawing\": false }");
			} catch (IOException e) {
				Utils.basicRedPrint("There was a problem creating necessary files.");
				Utils.error(e);
			}
			intro = true;
		} else {
			// If it does, read settings.json & apply settings.
			// Loop over all of the fields in the Settings class
			// and all of the values in settings.json, and apply
			// the values in settings.json to the fields in the Settings class.
			
			JSONObject settingsObject = new JSONObject(Utils.readFileInCWD("settings.json")); // Read settings file
			
			try {
				
				Class<?> settingsClass = Class.forName("io.sanger.henry.secondmemory.data.Settings"); // Get the Settings class
				
				for(String s : JSONObject.getNames(settingsObject)) { // Loop over all values in settings.json
					settingsClass.getField(s).set(null, settingsObject.get(s)); // For every value, set the corresponding value in the Settings class 
				}
				
			} catch (Exception e) {
				Utils.basicRedPrint("There was a problem loading settings.json.");
				Utils.error(e);
			}
		}
		
		// Check for topics.json
		if(!Utils.existsInCWD("topics.json")) {
			// If it doesn't exist, create default topics.json file
			Utils.createFileInCWD("topics.json");
			try {
				Utils.populateFileInCWD("topics.json", "{ \"topics\": [{\"name\": \"Default topic\",\"color\": \"cyan\"}] }");
			} catch (IOException e) {
				Utils.basicRedPrint("There was a problem creating necessary files.");
				Utils.error(e);
			}
			Utils.topics.add(new Topic("Default topic", TopicColor.CYAN));
		} else {
			// If it does, read topics.json & save topics
			JSONObject topicsObject = new JSONObject(Utils.readFileInCWD("topics.json"));
			JSONArray topicsArray = topicsObject.getJSONArray("topics");
			for(int i = 0; i < topicsArray.length(); i++) {
				JSONObject topic = topicsArray.getJSONObject(i);
				Topic newTopic = new Topic();
				newTopic.name = topic.getString("name");
				newTopic.color = TopicColor.parse(topic.getString("color"));
				Utils.topics.add(newTopic);
			}
		}
		
		// Check for questions.json
		if(!Utils.existsInCWD("questions.json")) {
			// If it doesn't exist, create an empty one
			Utils.createFileInCWD("questions.json");
			try {
				Utils.populateFileInCWD("questions.json", "{ \"questions\": [] }");
			} catch (IOException e) {
				Utils.basicRedPrint("There was a problem creating necessary files.");
				Utils.error(e);
			}
		} else {
			// If it does, load questions
			
			JSONObject questionsObject = new JSONObject(Utils.readFileInCWD("questions.json")); // Load questions.json
			JSONArray questionsArray = questionsObject.getJSONArray("questions"); // Get array of questions
			
			// Loop over every question in the array
			for(int i = 0; i < questionsArray.length(); i++) {
				
				Question finished = new Question(); // Instantiate new Question object
				JSONObject singleQuestion = questionsArray.getJSONObject(i); // Get content of JSON question
				
				// Copy properties from JSON to the new Question object
				finished.question = singleQuestion.getString("question");
				finished.answer = singleQuestion.getString("answer");
				finished.type = QuestionType.parse(singleQuestion.getString("type"));
				finished.topic = Utils.findTopic(singleQuestion.getString("topicName"));
				finished.latestScore = singleQuestion.getInt("latestScore");
				finished.latestLevenshtein = singleQuestion.getDouble("latestLevenshtein");
				JSONArray previousScores = singleQuestion.getJSONArray("previousScores");
				JSONArray previousLevenshteins = singleQuestion.getJSONArray("previousLevenshteins");
				for(int j = 0; j < previousScores.length(); j++) finished.scores.add(previousScores.getInt(j));
				for(int k = 0; k < previousLevenshteins.length(); k++) finished.allLevenshteins.add(previousLevenshteins.getDouble(k));
				
				finished.lastReview = Utils.parseDate(singleQuestion.getString("lastReview"));
				finished.nextReview = Utils.parseDate(singleQuestion.getString("nextReview"));
				finished.reviewedBefore = singleQuestion.getBoolean("reviewedBefore");
				
				list.questions.add(finished);
				
			}
		}
		
		Utils.clearScreen(); // Clear screen so it looks nicer
		
		
		// Put debug code here so it can be read easily without scrolling
		
		
		boolean first = true; // Variable that determines whether or not to show the header. Used to stop the header printing when the logo is.
		Utils.p("");
		// Logo
		Utils.p("\n" +
				"  ___                     _ __  __                        \n" + 
				" / __| ___ __ ___ _ _  __| |  \\/  |___ _ __  ___ _ _ _  _ \n" + 
				" \\__ \\/ -_) _/ _ \\ ' \\/ _` | |\\/| / -_) '  \\/ _ \\ '_| || |\n" + 
				" |___/\\___\\__\\___/_||_\\__,_|_|  |_\\___|_|_|_\\___/_|  \\_, |\n" + 
				"                                                     |__/ v" + Utils.PROGRAM_VERSION + "\n");
	
		// Intro. Only shows if the intro variable is set to true
		if(intro) {
			Utils.p("Welcome! It looks like this is the first time you've used SecondMemory.\n"
			+ "To get started, open online help by typing \"help\" (without the quotes) and Enter.\n"
			+ "If you're moving to SecondMemory from a different program, type \"i\" (without the quotes) and Enter\n"
			+ "to start the import wizard. It currently only supports Q&A files (like SuperMemo can export), but support for more programs is coming soon.");
		}
		
		// When the program quits, save data
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			Utils.osDependent(() -> AnsiConsole.systemUninstall(), () -> {});
			Utils.ps("\nSaving data...");
			try {
				Utils.populateFileInCWD("questions.json", list.toJSON());
			} catch (IOException e) {
				Utils.basicRedPrint("There was a problem saving the collection.");
				Utils.error(e);
			}
			Utils.p("Done.");
			Utils.p("Goodbye!");
		}));
		
		// Main loop
		while(true) {
			// Don't draw the header if the logo is showing.
			// Otherwise, do.
			if(first) {
				list.draw(false);
				first = false;
			} else {
				Utils.clearScreen();
				list.draw(true);
			}
			
			if(!floatText.equals("")) {
				// Print the float text
				if(floatTextErroneous) Utils.basicRedPrint(floatText);
				else if(floatTextSuccess) Utils.basicGreenPrint(floatText);
				else Utils.p(floatText);
				Utils.p();
				// Reset it
				floatText = "";
				floatTextErroneous = false;
				floatTextSuccess = false;
			}
			
			// Print list of commands, if option in settings.json is true
			if(Settings.showCommands) {
				Utils.p(("               ~ Basic Commands ~ \n"
				+ "              Question operations\n"
				+ "----------------------------------------------------------\n"
				+ "| \"f\" - New fact question | \"n\" - New narrative question |\n"
				+ "| \"e\" - Edit question     | \"d\" - Delete question        |\n"
				+ "| View question: Type its ID (located in the # column).  |\n"
				+ "----------------------------------------------------------\n"
				+ "                  Navigation\n"
				+ "------------------------------------------------\n"
				+ "| \",\" - Previous screen   | \".\" - Next screen  |\n"
				+ "| \",,\" - First screen     | \"..\" - Last screen |\n"
				+ "------------------------------------------------\n"
				+ "                    Other\n"
				+ "------------------------------------------------\n"
				+ "| \"q\" - Quit   | \"s\" - Save   | \"?\" - Help     |\n"
				+ "| \"h\" - Search | \"m\" - Mercy  | \"w\" - Workload |\n"
				+ "------------------------------------------------\n"
				+ "\"c\" for a list of all available commands.").replace("-", Settings.questionListTopChar).replace("|", Settings.questionListSideChar));
				Utils.p();
			}
			if(Settings.showWorkload) {
				if(!Settings.showCommands) Utils.p(); // Newline if commands aren't shown. No newline when they are, to prevent extra newlines
				Utils.drawWorkload(list.questions, 5);
			}
			// Print the prompt
			Utils.ps(Settings.prompt + " ");
			
			String userCommand = Utils.mainScanner.nextLine().toLowerCase(); // commands shouldn't be case-sensitive
			
			switch(userCommand) {
			
			// Help
			case "help":
			case "?":
				floatText = "Online help page opened.";
				floatTextSuccess = true;
				help();
				break;
				
			
			// Quit program
			case "q":
			case "quit":
			case "exit":
			case "leave":
				System.exit(0);
				break;
				
			// New question
			case "n":
				
				// Preparation
				Question question = new Question();
				File questionFile = new File(Utils.CWDS + "question.tmp");
				File tmpFile = new File(Utils.CWDS + "answer.tmp");
				if(tmpFile.exists()) tmpFile.delete(); // Delete old temporary file if it exists
				
				// Step 1
				Utils.clearScreen();
				Utils.header(63, "New Question");
				Utils.p();
				Utils.creationBreadcrumb.draw();
				Utils.creationBreadcrumb.nextItem();
				Utils.p("Type a number, or q to quit creation. Any other input will default to fact question:");
				TopicColor.printColor(TopicColor.CYAN);
				Utils.p("[1] - Fact question");
				TopicColor.printColor(TopicColor.MAGENTA);
				Utils.p("[2] - Narrative question");
				TopicColor.reset();
				Utils.ps("~> ");
				String type = Utils.mainScanner.nextLine();
				if(type.equalsIgnoreCase("q")) {
					if(Utils.confirmQuit("creation")) {
						floatText = "Question creation abandoned.";
						floatTextErroneous = true;
						Utils.creationBreadcrumb.reset();
						break;
					}
				}
				else if(type.equals("1") || type.equals("[1]")) { question.type = QuestionType.FACT; }
				else if(type.equals("2") || type.equals("[2]")) { question.type = QuestionType.NARRATIVE; }
				boolean isFactQuestion = question.type==QuestionType.FACT; // Used later to make a ternary easier
				
				// Step 2
				Utils.clearScreen();
				Utils.header(63, "New Question");
				Utils.p();
				Utils.creationBreadcrumb.draw();
				Utils.creationBreadcrumb.nextItem();
				Utils.ps("Type: ");
				TopicColor.printColor(isFactQuestion ? TopicColor.CYAN : TopicColor.MAGENTA);
				Utils.p(isFactQuestion ? "Fact" : "Narrative");
				TopicColor.reset();
				Utils.p();
				Utils.p("Step 2: Question");
				Utils.ps("Type a question, or:\n"
				+ "\"e\" - Open editor | \"q\" - Quit creation\n"
				+ "~> ");
				String givenQuestion = Utils.mainScanner.nextLine();
				boolean outerBreak = false;
				switch(givenQuestion) {
				case "e":
					Utils.createFileInCWD("question.tmp");
					Utils.p(Settings.autoMoveOn ? "Type your question in the editor, then close it."
							     : "");
					if(Settings.autoMoveOn) Utils.p("Waiting for the editor to close...");
					Utils.viewFileInCWD("question.tmp");
					if(!Settings.autoMoveOn) {
						Utils.ps("Editor opened. Press Enter when you're done.\n"
						 + "~> ");
						Utils.mainScanner.nextLine();
					}
					question.question = givenQuestion = Utils.readFileInCWD("question.tmp").trim();
					questionFile.delete();
					break;
				case "q":
					if(Utils.confirmQuit("creation")) {
						floatText = "Question creation abandoned.";
						floatTextErroneous = true;
						outerBreak = true;
						Utils.creationBreadcrumb.reset();
						break;
					}
				default:
					question.question = givenQuestion;
					break;
				}
				if(outerBreak) break;
				 
				// Step 3
				Utils.clearScreen();
				Utils.header(63, "New Question");
				Utils.p();
				Utils.creationBreadcrumb.draw();
				Utils.creationBreadcrumb.nextItem();
				Utils.ps("Type: ");
				TopicColor.printColor(isFactQuestion ? TopicColor.CYAN : TopicColor.MAGENTA);
				Utils.p(isFactQuestion ? "Fact" : "Narrative");
				TopicColor.reset();
				Utils.p();
				Utils.p("Question: " + givenQuestion);
				Utils.p();
				Utils.p("Step 3: Answer");
				Utils.ps("Type an answer to the question, or:\n"
				 + "\"e\" - Open editor | \"q\" - Quit creation\n"
				 + "~> ");
				String givenAnswer = Utils.mainScanner.nextLine();
				switch(givenAnswer) {
				case "e":
					Utils.createFileInCWD("answer.tmp");
					Utils.p(Settings.autoMoveOn ? "Type your answer in the editor, then close it." : "");
					if(Settings.autoMoveOn) Utils.p("Waiting for the editor to close...");
					Utils.viewFileInCWD("answer.tmp");
					if(!Settings.autoMoveOn) {
						Utils.ps("Editor opened. Press Enter when you're done.\n"
						 + "~> ");
						Utils.mainScanner.nextLine();
					}
					question.answer = givenAnswer = Utils.readFileInCWD("answer.tmp").trim();
					tmpFile.delete();
					break;
				case "q":
					if(Utils.confirmQuit("creation")) {
						floatText = "Question creation abandoned.";
						floatTextErroneous = true;
						outerBreak = true;
						Utils.creationBreadcrumb.reset();
						break;
					}
				default:
					question.answer = givenAnswer;
					break;
				}
				if(outerBreak) break;
				
				// Step 4
				Utils.clearScreen();
				Utils.header(63, "New Question");
				Utils.p();
				Utils.creationBreadcrumb.draw();
				Utils.creationBreadcrumb.nextItem();
				Utils.ps("Type: ");
				TopicColor.printColor(isFactQuestion ? TopicColor.CYAN : TopicColor.MAGENTA);
				Utils.p(isFactQuestion ? "Fact" : "Narrative");
				TopicColor.reset();
				Utils.p();
				Utils.p("Question: " + givenQuestion);
				Utils.p();
				Utils.p("Answer: " + givenAnswer);
				Utils.p();
				Utils.p("Step 3: Topic");
				question.topic = Utils.makeTopicChoice(false);
				if(question.topic == null) {
					floatText = "Question creation abandoned.";
					floatTextErroneous = true;
					Utils.creationBreadcrumb.reset();
					break;
				}
				
				// Add question to list.
				// 0 is needed because otherwise, the question will appear at the very end of the list.
				// This way, sorting is not necessary.
				list.questions.add(0, question); 
				
				// Update float text
				floatText = "Question was created successfully.";
				floatTextSuccess = true;
				Utils.creationBreadcrumb.reset();
				break;
				
			// Pagination
			case ".":
			case "next":
			case ">":
				list.nextScreen();
				break;
			case ",":
			case "previous":
			case "<":
				list.previousScreen();
				break;
			case "..":
			case "last":
			case ">>":
				
				list.lastScreen();
				break;
			case ",,":
			case "first":
			case "<<":
				list.firstScreen();
				break;
				
			// Save collection
			case "s":
			case "save":
				try {
					Utils.populateFileInCWD("questions.json", list.toJSON());
				} catch (IOException e1) {
					Utils.error(e1);
					floatText = "There was a problem saving the collection.";
					floatTextErroneous = true;
					break;		
				}
				floatText = "Save successful.";
				floatTextSuccess = true;
				break;
				
			// Edit question
			case "e":
			case "edit":
				while(true) {
					Utils.ps("Enter the number of the question to edit, or q to quit.\n"
					 + "~> ");
					String numToEdit = Utils.mainScanner.nextLine();
					try {
						if(!numToEdit.equals("q")) {
							int questionIndex = list.screenStart + Integer.parseInt(numToEdit);
							Question editedQuestion = editQuestion(list.questions.get(questionIndex));
							if(editedQuestion != null) list.questions.set(questionIndex, editedQuestion);
							break;
						} else {
							break;
						}
					} catch(Exception e) {
						Utils.basicRedPrint("That isn't a valid number. Press Enter and try again.");
						Utils.mainScanner.nextLine();
					}
				}
				break;
				
			// Delete question
			case "d":
			case "delete":
				deleteQuestion(list);
				break;
				
			// Search by name
			case "h":
			case "search":
				Utils.ps("Enter the search text.\n"
				 + "~> ");
				String filterText = Utils.mainScanner.nextLine().toLowerCase();
				QuestionList filtered = new QuestionList();
				for(Question question3 : list.questions) {
					if(question3.question.toLowerCase().matches(".*" + filterText + ".*") ||
					   question3.answer.toLowerCase().matches(".*" + filterText + ".*")) {
						filtered.questions.add(question3);
					}
				}
				boolean inLoop = true;
				String internalFloatText = "";
				boolean internalFloatTextErroneous = false;
				while(inLoop) {
					Utils.clearScreen();
					filtered.draw(true);
					Utils.p("Filtered by \"" + filterText + "\"\n");
					if(!internalFloatText.equals("")) {
						Utils.p();
						// Print the float text
						if(!internalFloatTextErroneous) Utils.p(internalFloatText);
						else Utils.basicRedPrint(internalFloatText);
						Utils.p();
						// Reset it
						internalFloatText = "";
						internalFloatTextErroneous = false;
					}
					if(Settings.showCommands) {
						Utils.p("                 Commands:\n"
						+ "				   Navigation\n"
						+ "--------------------------------------------------\n"
						+ "| \",\" - Previous screen | \".\" - Next screen  |\n"
						+ "| \",,\" - First screen   | \"..\" - Last screen |\n"
						+ "--------------------------------------------------\n"
						+ "                  Other\n"
						+ "--------------------------------------\n"
						+ "| \"?\" - Help | \"q\" - Quit search |\n"
						+ "--------------------------------------\n"
						+ "View question: Type its ID (located in the # column).");
					}
					Utils.ps(Settings.prompt + " ");
					String internalCommand = Utils.mainScanner.nextLine();
					switch(internalCommand) {
					case "help":
					case "?":
						help();
						break;
					case "q":
					case "quit":
					case "exit":
						inLoop = false;
						break;
					case ".":
						filtered.nextScreen();
						break;
					case ",":
						filtered.previousScreen();
						break;
					case "..":
						filtered.lastScreen();
						break;
					case ",,":
						filtered.firstScreen();
						break;
					default:
						// Check if command is a number
						if(internalCommand.matches("-?\\d+(\\.\\d+)?")) {
							// If it is, then review the corresponding question.
							// Add screenStart to the index so that the correct question is picked,
							// and subtract 1 (because the list starts at 1).
							try {
								Question reviewQuestion = filtered.questions.get(Integer.parseInt(internalCommand) - 1);
								viewQuestion(reviewQuestion, (Integer.parseInt(internalCommand) - 1));
							} catch(Exception e) {
								internalFloatText = "Invalid question number.";
								internalFloatTextErroneous = true;
							}
						} else {
							internalFloatText = "That isn't a valid command. Type \"help\" for help.";
							internalFloatTextErroneous = true;
						}
						break;
					}
				}
				break;
			
			// Import
			case "i":
			case "import":
				boolean outerBreak1 = false;
				Utils.clearScreen();
				Utils.header(32, "Import Questions");
				Utils.p();
				Utils.p("This feature is a work-in-progress, and currently only supports Q&A format (works with SuperMemo).");
				Utils.p("Step 1: File");
				ArrayList<Question> newQuestions = null;
				while(true) {
					Utils.ps("Press Enter to open a file chooser, or type the full path to the Q&A file. (or q to quit)\n"
					 + "~> ");
					String importInput = Utils.mainScanner.nextLine();
					if(importInput.equals("")) {
						JFileChooser chooser = new JFileChooser();
						chooser.showOpenDialog(null);
						File chosenFile = chooser.getSelectedFile();
						try {
							newQuestions = Importer.importQACollection(chosenFile);
						} catch(IOException ioe) {
							Utils.basicRedPrint("Couldn't process that file. It might not exist.\n"
									+ "Press Enter to try again.");
							Utils.mainScanner.nextLine();
							continue;
						}
						break;
					} else if(importInput.equals("q")) {
						floatText = "Import abandoned.";
						floatTextErroneous = true;
						outerBreak1 = true;
					} else {
						try {
							newQuestions = Importer.importQACollection(new File(importInput));
							break;
						} catch(IOException ioe) {
							Utils.basicRedPrint("Couldn't process that file. It might not exist.\n"
									+ "Press Enter to try again.");
							Utils.mainScanner.nextLine();
							continue;
						}
					}
					if(outerBreak1) break;
				}
				if(outerBreak1) break;
				Utils.clearScreen();
				Utils.header(32, "Import Questions");
				Utils.p();
				Utils.p("Step 2: Import Settings");
				Utils.ps("==Question scheduling options==\n"
				 + "\"s\" - Spread out questions over a given number of days\n"
				 + "\"d\" - Schedule them all for a given day\n"
				 + "\"l\" - Leave questions unscheduled (default)\n"
				 + "\"q\" - Exit import\n"
				 + "~> ");
				String settingsInput = Utils.mainScanner.nextLine();
				if(settingsInput.equals("q")) {
					floatText = "Import abandoned.";
					floatTextErroneous = true;
					break;
				}
				Utils.ps("Would you like to shuffle the questions (randomize their order)? [y]/n, or q to quit\n"
						 + "~> ");
				String shuffleInput = Utils.mainScanner.nextLine();
				if(shuffleInput.equalsIgnoreCase("q")) {
					floatText = "Import abandoned.";
					floatTextErroneous = true;
					break;
				}
				if(!shuffleInput.equalsIgnoreCase("n")) Collections.shuffle(newQuestions);
				boolean outerBreak2 = false;
				switch(settingsInput) {
				case "s":
					Utils.clearScreen();
					Utils.header(32);
					Utils.p();
					Utils.p("Step 3: Spread Settings");
					boolean validSpreadValueChosen = false;
					while(!validSpreadValueChosen) {
						Utils.ps("How many questions do you want per day? Choose a value between 1 and " + newQuestions.size() + " (inclusive), or q to quit.\n"
						 + "~> ");
						String perDayInput = Utils.mainScanner.nextLine();
						if(perDayInput.equals("q")) {
							floatText = "Import abandoned.";
							floatTextErroneous = true;
							outerBreak2 = true;
							break;
						}
						int perDay = 5;
						try {
							perDay = Integer.parseInt(perDayInput);
							if(perDay > newQuestions.size() || perDay < 1) throw new NumberFormatException();
						} catch(NumberFormatException nfe) {
							Utils.basicRedPrint("Invalid value. Press Enter and try again.");
							continue;
						}
						validSpreadValueChosen = true;
						// Calculate how many days the questions will need to be spread across
						LocalDate current = LocalDate.now();
						int questionsGivenToDay = 0;
						try (ProgressBar pb = new ProgressBar("Rescheduling questions", newQuestions.size(), ProgressBarStyle.ASCII)) {
							for(Question newQuestion : newQuestions) {
								newQuestion.reviewedBefore = true;
								newQuestion.nextReview = current;
								questionsGivenToDay++;
								if(questionsGivenToDay == perDay) {
									// Increment date by one
									LocalDate nextDay = current.plusDays(1);
									current = nextDay;
									questionsGivenToDay = 0;
								}
								pb.step();
							}
						}
					}
					break;
				case "d":
					Utils.clearScreen();
					Utils.header(32);
					Utils.p();
					Utils.p("Step 3: Scheduled Date");
					LocalDate scheduledDate = getDate("Enter the day you want to schedule the questions for, in a natural format (or q to quit).", false);
					if(scheduledDate != null) {
						for(Question newQuestion : newQuestions) {
							newQuestion.reviewedBefore = true;
							newQuestion.nextReview = scheduledDate;
						}
					} else {
						floatText = "Import abandoned.";
						floatTextErroneous = true;
						outerBreak2 = true;
					}
					break;
				}
				if(outerBreak2) break;
				try (ProgressBar pb = new ProgressBar("Importing questions", newQuestions.size(), ProgressBarStyle.ASCII)) {
					for(Question newQuestion : newQuestions) {
						list.questions.add(newQuestion);
						pb.step();
					}
				}
				Collections.sort(list.questions, Utils.sorter); // Resort questions when done
				floatText = "Your questions were imported successfully.";
				floatTextSuccess = true;
				break;
			case "w":
			case "workload":
				Utils.clearScreen();
				Utils.header(32);
				Utils.drawWorkload(list.questions, 30);
				Utils.ps("Press Enter to exit.\n"
				 + "~> ");
				Utils.mainScanner.nextLine();
				break;
			case "c":
			case "commands":
				allCommands();
				break;
			case "m":
			case "mercy":
				Utils.clearScreen();
				Utils.header(32);
				Utils.p("~ Mercy ~");
				int num = 0;
				boolean outerBreak3 = false;
				boolean pickedValidNumber = false;
				while(!pickedValidNumber) {
					Utils.ps("How many days forward or backward would you like to move your questions?\n"
					 + "A positive number will move questions forward, and a negative one will move them back.\n"
					 + "Type a number, or q to quit.\n"
					 + "~> ");
					String rawNum = Utils.mainScanner.nextLine();
					if(!rawNum.equals("q")) {
						try {
							num = Integer.parseInt(rawNum);
							pickedValidNumber = true;
						} catch(Exception e) {
							Utils.basicRedPrint("That isn't a valid number. Press Enter and try again.");
							Utils.mainScanner.nextLine();
							continue;
						}
					} else {
						floatText = "Mercy abandoned.";
						floatTextErroneous = true;
						outerBreak3 = true;
						break;
					}
				}
				if(outerBreak3) break;
				Utils.ps("You're about to move the due dates of all your questions by " + num + " day" + (num == 1 ? "" : "s") + ".\n"
				 + "Are you sure you want to do this? \"y\" to confirm, and anything else to cancel.\n"
				 + "~> ");
				String verification = Utils.mainScanner.nextLine();
				if(verification.equals("y")) {
					Utils.moveQuestions(num);
				} else {
					floatText = "Mercy abandoned.";
					floatTextErroneous = true;
				}
				break;
			case "":
				break;
			default:
				// Check if command is a number
				if(userCommand.matches("-?\\d+(\\.\\d+)?")) {
					// If it is, then review the corresponding question.
					// Add screenStart to the index so that the correct question is picked.
					try {
						Question reviewQuestion = list.questions.get(list.screenStart + Integer.parseInt(userCommand));
						viewQuestion(reviewQuestion, (Integer.parseInt(userCommand) - 1));
					} catch(Exception e) {
						floatText = "Invalid question number.";
						floatTextErroneous = true;
					}
				} else {
					// If not, then this isn't a valid command!
					floatText = "That isn't a valid command. Type \"help\" for help.";
					floatTextErroneous = true;
				}
				break;
			}
		}
		
	}
	
	// Opens online help.
	public static void help() {
		if(Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI("https://gitlab.com/gitlabcyclist/secondmemory/wikis/Help"));
				Utils.p("Opening online help.");
			} catch (Exception e) {
				Utils.error(e);
			}
		} else {
			 floatText = "Couldn't open help automatically.\n" +
					   "To view help, go to https://gitlab.com/gitlabcyclist/secondmemory/wikis/Help.";
			 floatTextErroneous = true;
		}
	}
	
	// Delete a question from the given QuestionList (almost always the main list).
	public static void deleteQuestion(QuestionList list) {
		Utils.clearScreen();
		list.draw(true, "Delete Question");
		Utils.ps("Enter the number of the question to delete, or q to quit.\n"
		 + "~> ");
		String toDeleteRaw = Utils.mainScanner.nextLine();
		if(!toDeleteRaw.equals("q")) {
			try {
				int rawQuestionInt = Integer.parseInt(toDeleteRaw.trim());
				if(rawQuestionInt > 9 || rawQuestionInt < 0) throw new IllegalArgumentException(); // trigger "That isn't a valid number"
				int questionID = (list.screenStart + rawQuestionInt);
				Utils.p("Are you sure? You're about to delete this question (this CANNOT be undone):");
				Utils.drawQuestion(list.questions.get(questionID));
				Utils.ps("Type \"y\" to accept, or anything else to cancel.\n"
					+ "~> ");
				if(Utils.mainScanner.nextLine().equalsIgnoreCase("y")) {
					list.questions.remove(questionID);
					floatText = "Question was deleted successfully.";
					floatTextSuccess = true;
				}
				else floatText = "Nothing was deleted.";
			} catch(IllegalArgumentException e) {
				Utils.basicRedPrint("That isn't a valid number. Press Enter and try again.");
				Utils.mainScanner.nextLine();
				deleteQuestion(list);
			} catch(Exception e) {
				Utils.error(e);
			}
		} else {
			floatText = "Nothing was deleted.";
		}
	}
	
	// Show a question, along with stats.
	public static void viewQuestion(Question question, int id) {
		Utils.clearScreen();
		Utils.header(32);
		Utils.p("QUESTION:");
		TopicColor.printColor(question.topic.color);
		Utils.ps("(" + question.topic.name + ") ");
		TopicColor.reset();
		Utils.p(question.question);
		String lastReviewPretty = (question.reviewedBefore ? Utils.jsonFormat(question.lastReview) : "not reviewed yet");
		String nextReviewPretty = (question.reviewedBefore ? Utils.jsonFormat(question.nextReview) : "not set");
		String typeStr = (question.type == QuestionType.FACT ? "Fact" : (question.type == QuestionType.NARRATIVE ? "Narrative" : "Unknown"));
		id++; // Make the question ID correct
		int lastReviewLength = lastReviewPretty.length() - 2;
		String firstLine = String.format("Type: %-" + (lastReviewLength + 9) + "s %s ID: %d\n",
										 typeStr, Settings.questionListSideChar, id),
			   secondLine = String.format("Latest score: %-" + (lastReviewLength + 1) + "d %s Latest Levenshtein: %.0f\n",
					   					  question.latestScore, Settings.questionListSideChar, question.latestLevenshtein),
			   thirdLine = String.format("Average score: %-" + lastReviewLength + ".2f %s Average Levenshtein: %.2f\n",
					                     question.calculateAverageScore(), Settings.questionListSideChar, question.calculateAverageLevenshtein()),
			   fourthLine = String.format("Last review: %s %s Next review: %-1s\n", lastReviewPretty, Settings.questionListSideChar, nextReviewPretty);
		int[] lengthArray = new int[4];
		lengthArray[0] = firstLine.length();
		lengthArray[1] = secondLine.length();
		lengthArray[2] = thirdLine.length();
		lengthArray[3] = fourthLine.length();
		int longest = 0;
		for(int i = 0; i < 4; i++) if(lengthArray[i] > longest) longest = lengthArray[i];
		for(int i = 0; i < longest - 1; i++) Utils.ps(Settings.questionListTopChar);
		Utils.p();
		Utils.ps(firstLine + secondLine + thirdLine + fourthLine);
		for(int i = 0; i < longest - 1; i++) Utils.ps(Settings.questionListTopChar);
		Utils.p();
		if(Settings.showCommands) {
			Utils.p("                Commands");
			Utils.p("---------------------------------------");
			Utils.p("| \"r\" - Review | Any other key - exit |");
			Utils.p("---------------------------------------");
		}
		Utils.ps("~> ");
		String input = Utils.mainScanner.nextLine();
		if(input.toLowerCase().equals("r")) reviewQuestion(question);
	}
	
	// Review a question.
	public static void reviewQuestion(Question question) {
		if(question.type == QuestionType.FACT) {
			Utils.clearScreen();
			Utils.header(32);
			Utils.p("QUESTION:");
			Utils.drawQuestion(question);
			Utils.ps("Enter your answer, or q to quit: ");
			String yourAnswer = Utils.mainScanner.nextLine();
			if(yourAnswer.equalsIgnoreCase("q")) {
				floatText = "Question review abandoned.";
				floatTextErroneous = true;
				return;
			}
			Utils.p("CORRECT ANSWER:\n" + question.answer);
			Utils.p("\nLevenshtein distance: " + Utils.similarity(question.answer, yourAnswer));
			int score = getScore();
			if(score == -1) {
				floatText = "Question review abandoned.";
				floatTextErroneous = true;
				return;
			}
			Utils.p("Workload:");
			Utils.drawWorkload(list.questions, 10);
			LocalDate date = getReviewDate();
			if(date == null) {
				floatText = "Question review abandoned.";
				floatTextErroneous = true;
				return;
			}
			question.score(score, yourAnswer);
			question.changeDates(date);
			floatText = "Question review successful. There are now " + Utils.questionsForDate(date) + " questions due on the scheduled day.";
			floatTextSuccess = true;
		} else if(question.type == QuestionType.NARRATIVE) {
			Utils.clearScreen();
			Utils.header(32);
			Utils.p("QUESTION:");
			Utils.drawQuestion(question);
			Utils.ps("Press Enter and type your answer, or q to quit.\n"
			 + "~> ");
			String input = Utils.mainScanner.nextLine();
			if(input.equalsIgnoreCase("q")) {
				floatText = "Question review abandoned.";
				floatTextErroneous = true;
				return;
			}
			
			File answerFile = new File("answer.tmp");
			if(answerFile.exists()) answerFile.delete(); // Delete temp file
			Utils.createFileInCWD("answer.tmp");
			if(Settings.autoMoveOn) Utils.p("Waiting for the editor to close...");
			Utils.viewFileInCWD("answer.tmp");
			
			if(!Settings.autoMoveOn) {
				Utils.ps("Editor opened. Press Enter when you're done.\n"
				 + "~> ");
				Utils.mainScanner.nextLine();
			}
			String userAnswer = Utils.readFileInCWD("answer.tmp");
			answerFile.delete();
			Utils.p("\nYOUR ANSWER:\n" + userAnswer + "\nMODEL:\n" + question.answer);
			Utils.p("Levenshtein distance: " + Utils.similarity(question.answer, userAnswer));
			int score = getScore();
			if(score == -1) {
				floatText = "Question review abandoned.";
				floatTextErroneous = true;
				return;
			}
			LocalDate date = getReviewDate();
			if(date == null) {
				floatText = "Question review abandoned.";
				floatTextErroneous = true;
				return;
			}
			question.score(score, userAnswer);
			question.changeDates(date);
			floatText = "Question review successful. There are now " + Utils.questionsForDate(date) + " questions due on the scheduled day.";
			floatTextSuccess = true;
		} else if(question.type == QuestionType.IMPORTED_FACT_SUGGESTED) {
			Utils.basicRedPrint("Not supported yet. Press Enter to continue.");
			Utils.mainScanner.nextLine();
		} else {
			Utils.basicRedPrint("Invalid question.");
		}
		Collections.sort(list.questions, Utils.sorter);
		
	}
	
	public static void allCommands() {
		int page = 0;
		while(true) {
			Utils.clearScreen();
			Utils.header(32);
			Utils.p("~ All Commands ~\n"
							 + "Page " + (page+1) + "\n"
					
						     + (page == 0 ?
						       "-- Command List Navigation -- \n"
						     + "======================================\n"
						     + "\",\" - Previous page | \".\" - Next page\n"
						     + "\",,\" - First page   | \"..\" - Last page\n"
						     + "======================================\n"
						     			  : "")
						    
							+ "");
			switch(page) {
			case 0:
				Utils.p("-- Question Operations --\n"
			    + "==============================================================\n"
			    + "\"f\" - New fact question | \"n\" - New narrative question\n"
			    + "\"e\" - Edits a question  | \"d\" - Deletes a question\n"
			    + "To view a question, type its ID; it's located in the # column.\n"
			    + "==============================================================\n"
			    + "\n"
			    + "-- Rescheduling --\n"
			    + "=======================================================================\n"
			    + "\"m\" - Mercy | \"w\" - Workload | \"r\" - Reschedule question (coming soon!)\n"
			    + "=======================================================================");
				break;
			case 1:
				Utils.p("-- Navigation & Search --\n"
				+ "=============================================================\n"
				+ "\",\" - Previous screen  | \".\" - Next screen\n"
				+ "\",,\" - First screen    | \"..\" - Last screen\n"
				+ "\"h\" - Search by name   | \"t\" - Search by topic (coming soon!)\n"
				+ "=============================================================");
				Utils.p("-- More --\n"
				+ "====================================\n"
				+ "\"s\" - Save | \"?\" - Help | \"q\" - Quit\n"
				+ "====================================\n");
				break;
			}
			Utils.ps(Settings.prompt + " ");
			String command = Utils.mainScanner.nextLine();
			switch(command) {
			case ",,":
				page = 0;
				break;
			case ",":
				if(page > 0) page--;
				break;
			case ".":
				if(page < 2) page++;
				break;
			case "..":
				page = 1;
				break;
			case "q":
				return;
			}
		}
	} 
	
	// This method was intended for use with imported questions.
	// However, the relevant logic was never implemented, so this
	// method is not used for anything at the moment.
	public static void importedQuestionWizard(Question question) {
		String questionTypeName = "";
		switch(question.type) {
		case IMPORTED_FACT_SUGGESTED:
			questionTypeName = "fact";
			break;
		case IMPORTED_NARRATIVE_SUGGESTED:
			questionTypeName = "narrative";
			break;
		default:
			break;
		}
		Utils.clearScreen();
		Utils.header(32);
		Utils.p("This question was imported, and the type (fact or narrative) hasn't been set.\n"
		 + "SecondMemory suggests that you make this question a " + questionTypeName + " question.\n"
		 + "Press Enter to go along with this suggestion, or:\n\"n\" - make this question a narrative question\n\"f\" - make this question a fact question");
	}
	
	// Prints 2 questions. Used for the editQuestion(Question) method.
	public static void printQuestions(Question one, Question two) {
		Utils.p();
		Utils.p("Original question:");
		Utils.drawQuestion(one);
		Utils.p();
		Utils.p("Edited question:");
		Utils.drawQuestion(two);
		Utils.p();
	}
	
	// Edits a question.
	public static Question editQuestion(Question question) {
		
		// Preparations
		
		// Prepare temporary files
		File questionFile = new File(Utils.CWDS + "question.tmp");
		File answerFile = new File(Utils.CWDS + "answer.tmp");
		
		if(answerFile.exists()) answerFile.delete(); // Delete old temporary file if it exists
		Question finishedQuestion = question.clone(); // Make a copy of the given question to modify
		
		// Step 1
		Utils.clearScreen();
		Utils.header(63, "Editing");
		printQuestions(question, finishedQuestion);
		Utils.p("Step 1: Question");
		Utils.ps("Type a new question, or:\n"
		+ "Enter - move on without changing the question\n"
		+ "\"e\" - Open editor with existing question text\n"
		+ "\"q\" - Quit editing\n"
		+ "~> ");
		String input = Utils.mainScanner.nextLine();
		boolean outerBreak = false;
		switch(input) {
		case "e":
			Utils.createFileInCWD("question.tmp");
			try {
				Utils.populateFileInCWD("question.tmp", question.question);
			} catch (IOException e) {
				Utils.basicRedPrint("There was a problem writing to a file.");
				Utils.error(e);
			}
			Utils.ps((Settings.autoMoveOn ? "Waiting for the editor to close..."
						   : "Type your question in the editor, and press Enter when you're done.\n~> "));
			Utils.viewFileInCWD("question.tmp");
			if(!Settings.autoMoveOn) Utils.mainScanner.nextLine();
			finishedQuestion.question = Utils.readFileInCWD("question.tmp").trim();
			questionFile.delete();
			break;
		case "q":
			floatText = "Question editing abandoned.";
			floatTextErroneous = true;
			outerBreak = true;
			break;
		case "":
			break;
		default:
			finishedQuestion.question = input;
			break;
		}
		if(outerBreak) return null;
		
		
		// Step 2
		Utils.clearScreen();
		Utils.header(63, "Editing");
		printQuestions(question, finishedQuestion);
		Utils.p("Step 2: Answer");
		Utils.ps("Type e to modify the answer, or:\n"
		 + "Any key or Enter - move on without changing it\n"
		 + "q - Quit editing\n"
		 + "~> ");
		String answerInput = Utils.mainScanner.nextLine();
		outerBreak = false;
		switch(answerInput) {
		case "e":
			Utils.createFileInCWD("answer.tmp");
			try {
				Utils.populateFileInCWD("answer.tmp", question.answer);
			} catch (IOException e) {
				Utils.basicRedPrint("There was a problem writing to a file.");
				Utils.error(e);
			}
			Utils.viewFileInCWD("answer.tmp");
			if(Settings.autoMoveOn) {
				Utils.p("Waiting for the editor to close...");
			} else {
				Utils.ps("Editor opened. Press Enter when you're done.\n"
				 + "~> ");
				Utils.mainScanner.nextLine();
			}
			finishedQuestion.answer = Utils.readFileInCWD("answer.tmp");
			answerFile.delete();
			break;
		case "":
			break;
		case "q":
			floatText = "Question editing abandoned.";
			floatTextErroneous = true;
			outerBreak = true;
			break;
		}
		if(outerBreak) return null;
		
		// Step 3
		Utils.clearScreen();
		Utils.header(63);
		printQuestions(question, finishedQuestion);
		Utils.p("Step 3: Topic");
		Topic topic = Utils.makeTopicChoice(true);
		if(topic != null) {
			// If staySame is false, change the topic based on the choice;
			// otherwise, don't change the topic.
			if(!topic.staySame)
				finishedQuestion.topic = topic;
		} else {
			floatText = "Question editing abandoned.";
			floatTextErroneous = true;
			return null;
		}
		
		Utils.clearScreen();
		Utils.header(63);
		Utils.p();
		Utils.p("Editing:");
		Utils.drawQuestion(question);
		Utils.p();
		Utils.p("Step 4: Type");
		Utils.p("Type a number to select a type, or:\n"
		+ "Enter - move on without changing type\n"
		+ "q - quit editing");
		TopicColor.printColor(TopicColor.CYAN);
		Utils.p("(1) Fact");
		TopicColor.printColor(TopicColor.MAGENTA);
		Utils.p("(2) Narrative");
		TopicColor.reset();
		Utils.ps("~> ");
		String type = Utils.mainScanner.nextLine();
		if(type.equalsIgnoreCase("q")) {
			floatText = "Question editing abandoned.";
			floatTextErroneous = true;
			return null;
		}
		else if(type.equals("1")) { finishedQuestion.type = QuestionType.FACT; }
		else if(type.equals("2")) { finishedQuestion.type = QuestionType.NARRATIVE; }
		
		
		floatText = "Question was edited successfully.";
		floatTextSuccess = true;
		return finishedQuestion;
	}
	
	private static int getScore() {
		Utils.ps("Enter a score from 1 to 5 (see help for details), or q to quit.\n"
		 + "~> ");
		String input = Utils.mainScanner.nextLine();
		try {
			if(!input.equals("q")) {
				int score = Integer.parseInt(input);
				if(score > 0 && score < 6) return score;
				else {
					Utils.basicRedPrint("That isn't a valid score. Press Enter and try again.");
					Utils.mainScanner.nextLine();
					return getScore();
				}
			}
			else return -1;
		} catch(NumberFormatException nfe) {
			Utils.basicRedPrint("That isn't a valid score. Press Enter and try again.");
			Utils.mainScanner.nextLine();
			return getScore();
		}
	}
	
	private static LocalDate getDate(String message, boolean isReviewDate) {
		Utils.ps(message + "\n"
					 + "~> ");
		String rawDate = Utils.mainScanner.nextLine();
		if(!rawDate.equals("q")) {
			LocalDate nextReview = Utils.parseDate(rawDate);
			int questionsOnScheduledDate = Utils.questionsForDate(nextReview);
			int amountOverLimit = (questionsOnScheduledDate - Settings.questionLimit);
			// Check if date is over the limit
			if(isReviewDate && (questionsOnScheduledDate + 1 > Settings.questionLimit)) { // + 1 to check if limit will be exceeded once question is scheduled
				// If it is, print warning
				Utils.basicRedPrint("Warning: There are " + questionsOnScheduledDate + " questions on that date,\n"
						    + "which " + (amountOverLimit == 0 ? "will exceed" : "is " + amountOverLimit + " over") + " the limit.\n"
						    + "Are you sure you want to schedule this question for that date?");
				Utils.ps("Type y to continue, or anything else to pick a new date.\n"
				 + "~> ");
				String pickNewDate = Utils.mainScanner.nextLine(); // Get input
				if(pickNewDate.equals("y")) {
					// If confirmed anyway, schedule
					return nextReview;
				} else {
					// If not, try again
					return getDate(message, isReviewDate);
				}
			}
			if(nextReview == null) {
				Utils.basicRedPrint("That isn't a valid date. Press Enter and try again.");
				Utils.mainScanner.nextLine();
				return getDate(message, isReviewDate);
			}
			return nextReview;
		} else {
			return null;
		}
	}
	
	private static LocalDate getReviewDate() {
		return getDate("Enter the next review date, in a natural format (or q to quit).", true);
	}

}