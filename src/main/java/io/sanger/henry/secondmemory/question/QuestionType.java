package io.sanger.henry.secondmemory.question;

public enum QuestionType {
	FACT, NARRATIVE, IMPORTED_NARRATIVE_SUGGESTED, IMPORTED_FACT_SUGGESTED, IMPORTED_NARRATIVE, IMPORTED_FACT;
	
	public static String toString(QuestionType type) {
		switch(type) {
		case FACT:
			return "fact";
		case NARRATIVE:
			return "narrative";
		case IMPORTED_NARRATIVE:
			return "narrative (imported)";
		case IMPORTED_FACT:
			return "fact (imported)";
		case IMPORTED_NARRATIVE_SUGGESTED:
			return "narrative suggested (imported)";
		case IMPORTED_FACT_SUGGESTED:
			return "fact suggested (imported)";
		}
		return "unknown";
	}
	
	public static QuestionType parse(String type) {
		if(type.equals("fact")) return FACT;
		else if(type.equals("narrative")) return NARRATIVE;
		else if(type.equals("Fact (imported)")) return IMPORTED_FACT;
		else if(type.equals("Fact (imported; )")) return IMPORTED_FACT;
		else if(type.equals("narrative (imported)")) return IMPORTED_NARRATIVE;
		else if(type.equals("fact (imported)")) return IMPORTED_FACT;
		else if(type.equals("narrative suggested (imported)")) return IMPORTED_NARRATIVE_SUGGESTED;
		else if(type.equals("fact suggested (imported)")) return IMPORTED_FACT_SUGGESTED;
		else return null;
	}
	
}
