package io.sanger.henry.secondmemory;

public class Breadcrumb {
	
	private String[] parts;
	
	private int highlightedId = 0;
	
	public Breadcrumb(String...parts) {
		this.parts = parts;
	}
	
	public void reset() {
		highlightedId = 0;
	}
	
	public void nextItem() {
		if(highlightedId + 1 < parts.length) // Check to make sure the index of the highlighted part will actually be in the array
			highlightedId++;
	}
	
	public void draw() {
		Utils.p("Step " + (highlightedId+1) + "/" + parts.length);
		for(int i = 0; i < parts.length; i++) {
			String part = parts[i];
			TopicColor color = (highlightedId == i ? TopicColor.YELLOW : TopicColor.MAGENTA);
			TopicColor.printColor(color);
			Utils.ps(part);
			TopicColor.reset();
			if(!(i == parts.length - 1)) {
				TopicColor.printColor(TopicColor.LIGHT_BLUE);
				Utils.ps(" => ");
				TopicColor.reset();
			}
		}
		Utils.p();
		Utils.p();
	}
	
}
