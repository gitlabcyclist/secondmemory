#!/bin/bash
echo "creating binary..."
./generateLinuxBinary.sh
echo "installing binary..."
cd ..
sudo mv ./linuxBinary/secondmemory /usr/bin/secondmemory
echo "done."
echo "installing desktop file & icon..."
cd ubuntuInstallTools
cp ./SecondMemory.desktop ~/.local/share/applications/SecondMemory.desktop
chmod +x ~/.local/share/applications/SecondMemory.desktop
cp ./SecondMemory.png ~/.local/share/icons/SecondMemory.png
echo "done."
