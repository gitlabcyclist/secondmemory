#!/bin/bash
pwd
cd ..
echo "generating jar..."
./gradlew fatJar
mv ./build/libs/SecondMemory.jar ./linuxBinary/SecondMemory.jar
echo "done."
echo "creating binary..."
cd linuxBinary
cat stub.sh SecondMemory.jar > secondmemory
chmod +x secondmemory
cd ..
echo "done."
